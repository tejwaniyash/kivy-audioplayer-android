import kivy
import android
import os
from itertools import chain
from jnius import autoclass
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.behaviors import FocusBehavior
from kivy.properties import BooleanProperty,ObjectProperty
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen,ScreenManager
from kivy.core.audio import SoundLoader
from kivy.core.window import Window

Builder.load_string('''
<SelectableLabel>:
    # Draw a background to indicate selection
    canvas.before:
        Color:
            rgba: (.0, 0.9, .1, .3) if self.selected else (0, 0, 0, 1)
        Rectangle:
            pos: self.pos
            size: self.size
               
<RV>:
    viewclass: 'SelectableLabel'
    SelectableRecycleBoxLayout:
        default_size: None, dp(56)
        default_size_hint: 1, None
        size_hint_y: None
        height: self.minimum_height
        orientation: 'vertical'
        multiselect: False
        touch_multiselect: False

   
<AudioScreen>: 
    GridLayout:
        rows:1
        pos:0,50
        size:root.width,100
        row_default_height:100
        row_force_default: True
        Button: 
            text: '<--'
            background_color: 0,0,0,1
            on_release:root.prev_playback()
        Button:
            id : play_pause_bt
           
            background_color: 0,0,0,1
            on_release: root.stop_playback()
        Button:
            text: '-->'
            background_color: 0, 0, 0, 1 
            on_release: root.next_playback()          
    Label:
        id: status
        pos: 0,0
        center:root.center 
''')

files = []
filename = []


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''


class SelectableLabel(RecycleDataViewBehavior, Label):
    ''' Add selection support to the Label '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableLabel, self).refresh_view_attrs(
            rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if is_selected:
            MainObj.ado_page.view(index)
            MainObj.sm.current = 'ado'
            # self.selected = false

class RV(RecycleView):
    def __init__(self, **kwargs):
        super(RV, self).__init__(**kwargs)
        for r, d, f in os.walk('/sdcard/'):
            for file in f:
                if '.mp3' in file:
                    files.append(os.path.join(r,file))
                    filename.append(file)
                    self.data = [{'text': str(f)} for f in filename]

                
class AudioScreen(Widget):
    def stop_playback(self):
        if self.isPlaying:
            self.mplayer.pause();
            self.ids.play_pause_bt.text = '>'
            self.isPlaying = False
        else:
            self.mplayer.start()
            self.ids.play_pause_bt.text = '||'
            self.isPlaying = True

    def view(self,source):
        if source < 0:
            self.ids.status.text = "No Music to Play"
        else:
            self.isPlaying = False
            MediaPlayer = autoclass('android.media.MediaPlayer')
            self.mplayer = MediaPlayer()
            self.index2 = source
            self.source = files[source]
            self.mplayer.setDataSource(self.source)
            self.mplayer.prepare()
            self.mplayer.start()
            self.isPlaying = True
            self.ids.status.text = filename[self.index2]
            if self.isPlaying:
                self.ids.play_pause_bt.text = '||'
            else:
                self.ids.play_pause_bt.text = '>'
                
                
    def prev_playback(self):
        self.mplayer.stop()
        self.mplayer.reset()
        self.index2 = self.index2  - 1
        #self.view(self,index2-1)
        self.mplayer.setDataSource(files[self.index2])
        self.mplayer.prepare()
        self.mplayer.start()
        self.isPlaying = True
        self.ids.status.text = filename[self.index2]
        if self.isPlaying:
            self.ids.play_pause_bt.text = '||'
        else:
            self.ids.play_pause_bt.text = '>'
            
                                   
    def next_playback(self):
        self.mplayer.stop()
        self.mplayer.reset()
        self.index2 = self.index2 + 1
        #self.view(self,index2+1)
        self.mplayer.setDataSource(files[self.index2])
        self.mplayer.prepare()
        self.mplayer.start()
        self.isPlaying = True
        self.ids.status.text = filename[self.index2]
        if self.isPlaying:
            self.ids.play_pause_bt.text = '||'
        else:
            self.ids.play_pause_bt.text = '>'
            
        
class TestApp(App):
    def __init__(self, **kwargs):
        super(TestApp,self).__init__(**kwargs)
    #android.map_key(android.KEYCODE_BACK,1001)
        Window.bind(on_keyboard = self.onBackButton)

    def onBackButton(self,window,key,*largs):
        if key == android.KEYCODE_BACK:
            MainObj.sm.current = 'RV'
            return True
        return False
        
    def build(self):
        self.sm = ScreenManager()

        self.rv_page = RV()
        screen = Screen(name ='RV')
        screen.add_widget(self.rv_page)
        self.sm.add_widget(screen)
        
        self.ado_page = AudioScreen()
        screen = Screen(name ='ado')
        screen.add_widget(self.ado_page)
        self.sm.add_widget(screen)
        
        return self.sm

if __name__ == '__main__':
    MainObj = TestApp()
    MainObj.run()
    
